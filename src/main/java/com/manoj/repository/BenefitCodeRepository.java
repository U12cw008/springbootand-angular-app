package com.manoj.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.manoj.model.BenefitCode;

@Repository
public interface BenefitCodeRepository
		extends JpaRepository<BenefitCode, Integer>, JpaSpecificationExecutor<BenefitCode> {

	@Query("select b from BenefitCode b where b.id =:id")
	public BenefitCode findOne(@Param("id") Integer id);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update BenefitCode b set b.isDeleted =:deleted where b.id =:id")
	public void deleteById(@Param("deleted") boolean deleted, @Param("id") Integer id);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update BenefitCode b set b.isDeleted =:deleted where b.id =:id")
	public void deleteSingleBenefitCode(@Param("deleted") boolean deleted, @Param("id") Integer id);

}
