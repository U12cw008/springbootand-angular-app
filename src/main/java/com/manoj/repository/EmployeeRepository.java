package com.manoj.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.manoj.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer>, JpaSpecificationExecutor<Employee> {

	@Query("select e from Employee e where e.id=:id")
	public Employee findOne(@Param("id") Integer id);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Employee  e set e.isDeleted =:deleted where e.id =:id")
	public void deleteById(@Param("deleted") boolean deleted, @Param("id") Integer id);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Employee e set e.isDeleted =:deleted where e.id =:id")
	public void deleSingleRecord(@Param("deleted") boolean deleted, @Param("id") Integer id);

}
