package com.manoj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manoj.model.Applicant;

@Repository
public interface ApplicantRepository extends JpaRepository<Applicant, Integer> {

}
