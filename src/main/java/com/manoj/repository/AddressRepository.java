package com.manoj.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.manoj.model.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer>, JpaSpecificationExecutor<Address> {

	@Query("select a from Address a where a.id =:id")
	public Address findOne(@Param("id") Integer id);

	@Query("update Address a set a.isDeleted =:deleted where a.id =:id")
	public void deleteById(@Param("deleted") boolean deleted, @Param("id") int id);

	@Query("select a from Address a where (a.countryId Like :searchKey or a.countryName Like :searchKey or a.cityName Like:searchKey or a.stateName Like :searchKey)")
	public Page<Address> searchData(@Param("searchKey") String searchKey, Pageable pageable);

	@Query("update Address a set a.isDeleted =:deleted where a.id =:id")
	public void deleteSinglerecord(@Param("deleted") boolean deleted, @Param("id") Integer id);
}
