package com.manoj.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.opencsv.bean.CsvBindByName;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "HR001")
@Where(clause = "is_deleted=0")
public class Department {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@CsvBindByName
	@Column(name = "DEPINDX")
	private int id;

	@CsvBindByName
	@Column(name = "DEPID")
	private String depeartmentId;

	@CsvBindByName
	@Column(name = "DEPNAME")
	private String departmentName;

	@CsvBindByName
	protected Boolean isDeleted;

}
