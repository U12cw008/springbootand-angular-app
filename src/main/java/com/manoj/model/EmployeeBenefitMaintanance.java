package com.manoj.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "HR006")
public class EmployeeBenefitMaintanance {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPBENINDEXID")
	private int id;

	@Column(name = "BENNAMEFOREMP")
	private String benefitName;

	@Column(name = "BENCSTDT")
	private LocalDate startDate;

	@Column(name = "BENCEDDT")
	private LocalDate endDate;

	@Column(name = "BENCAMTYR", precision = 10, scale = 3)
	private BigDecimal benefitAmountPerYear;

	@Column(name = "BENCAMTMNTH", precision = 10, scale = 3)
	private BigDecimal benefitAmountPerMonth;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="BENINDEXID")
	private BenefitCode benefitCode;

}
