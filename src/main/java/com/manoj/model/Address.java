package com.manoj.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import lombok.Data;

@Data
@Entity
@Table(name = "HR003")
@Where(clause = "is_deleted=0")
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "INDEXID")
	private int id;

	@Column(name = "CNTRYID")
	private String countryId;
	@Column(name = "CNTRYNAME")
	private String countryName;
	@Column(name = "CITYNAME")
	private String cityName;
	@Column(name = "STATENAME")
	private String stateName;
	@Column(name = "ZEPCODE")
	private Integer zepCode;
	@Column(name = "is_deleted")
	private boolean isDeleted;

}
