package com.manoj.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "HR007")
public class Applicant {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "APPLICINDX")
	private int id;

	@Column(name = "FRSTNAM", columnDefinition = "char(31)")
	private String firstName;

	@Column(name = "LSTNAME", columnDefinition = "char(31)")
	private String lastName;

	@Column(name = "ADDRSS", columnDefinition = "char(60)")
	private String address;

	@Column(name = "CITY", columnDefinition = "char(21)")
	private String city;

	@Column(name = "COUNTRY", columnDefinition = "char(31)")
	private String country;

	@Column(name = "PHONE", columnDefinition = "char(21)")
	private String phone;

	@Column(name = "APLICANTTYPE")
	private Boolean applicantType;
}
