package com.manoj.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "HR004")
public class BenefitCode {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "BENINDEXID")
	private int id;
	@Column(name = "BENEFITID")
	private String benefitId;
	@Column(name = "BENEFITCODE")
	private String benefitCode;
	@Column(name = "BENEFITDESC")
	private String benefitDesc;
	@Column(name = "is_deleted")
	private boolean isDeleted;
	
	@OneToMany(mappedBy="benefitCode")
	private List<EmployeeBenefitMaintanance> employeeBenefitMaintananceList;
}
