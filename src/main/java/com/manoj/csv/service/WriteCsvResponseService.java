package com.manoj.csv.service;

import java.io.PrintWriter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.manoj.model.Employee;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvException;

@Service
public class WriteCsvResponseService {

	private static final Logger LOGGER = LoggerFactory.getLogger(WriteCsvResponseService.class);

	public static  void writeCities(PrintWriter writer, List<Employee> employee) {

		try {

			ColumnPositionMappingStrategy<Employee> mapStrategy = new ColumnPositionMappingStrategy<>();

			mapStrategy.setType(Employee.class);

			String[] columns = new String[] { "id", "empId", "empFirstName", "empGender", "empType" };
			mapStrategy.setColumnMapping(columns);

			StatefulBeanToCsv<Employee> btcsv = new StatefulBeanToCsvBuilder<Employee>(writer)
					.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).withMappingStrategy(mapStrategy).withSeparator(',')
					.build();

			btcsv.write(employee);

		} catch (CsvException ex) {

			LOGGER.error("Error mapping Bean to CSV", ex);
		}
	}

	public static void writeCity(PrintWriter writer, Employee city) {

		try {

			ColumnPositionMappingStrategy<Employee> mapStrategy = new ColumnPositionMappingStrategy<>();

			mapStrategy.setType(Employee.class);

			String[] columns = new String[] { "id", "empId", "empFirstName", "empGender", "empType" };
			mapStrategy.setColumnMapping(columns);

			StatefulBeanToCsv<Employee> btcsv = new StatefulBeanToCsvBuilder<Employee>(writer)
					.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).withMappingStrategy(mapStrategy).withSeparator(',')
					.build();

			btcsv.write(city);

		} catch (CsvException ex) {

			LOGGER.error("Error mapping Bean to CSV", ex);
		}
	}

}
