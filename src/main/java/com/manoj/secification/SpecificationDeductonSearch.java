package com.manoj.secification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.manoj.model.DeductionCode;
import com.manoj.search.dto.DeductionSearchDto;

public class SpecificationDeductonSearch implements Specification<DeductionCode> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private DeductionSearchDto deductionSearchDto;

	public SpecificationDeductonSearch(DeductionSearchDto deductionSearchDto) {
		this.deductionSearchDto = deductionSearchDto;
	}

	@Override
	public Predicate toPredicate(Root<DeductionCode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

		List<Predicate> predicates = new ArrayList<Predicate>();
		List<Predicate> orPredicates = new ArrayList<Predicate>();
		Predicate[] predicateArray = new Predicate[predicates.size()];
		Predicate and = criteriaBuilder.and(predicates.toArray(predicateArray));
		Predicate finalPredicate = null;

		if (deductionSearchDto.getSearchKey() != null) {
			orPredicates
					.add(criteriaBuilder.like(root.get("deductionId"), "%" + deductionSearchDto.getSearchKey() + "%"));
			orPredicates.add(
					criteriaBuilder.like(root.get("deductionName"), "%" + deductionSearchDto.getSearchKey() + "%"));
			orPredicates.add(
					criteriaBuilder.like(root.get("deductionCode"), "%" + deductionSearchDto.getSearchKey() + "%"));
			orPredicates.add(
					criteriaBuilder.like(root.get("deductionDesc"), "%" + deductionSearchDto.getSearchKey() + "%"));
			Predicate[] orPredicateArray = new Predicate[predicates.size()];
			Predicate or = criteriaBuilder.or(orPredicates.toArray(orPredicateArray));
			finalPredicate = criteriaBuilder.and(and, or);
		} else {
			finalPredicate = and;
		}
		return criteriaBuilder.and(finalPredicate);
	}

}
