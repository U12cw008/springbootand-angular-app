package com.manoj.secification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.manoj.model.Department;
import com.manoj.search.dto.DepartmentSearchDto;

public class SpecificationDepartmentSearch implements Specification<Department>{
	
	private static final long serialVersionUID = 1L;
	private DepartmentSearchDto searchDto;
	
	public SpecificationDepartmentSearch(DepartmentSearchDto searchDto) {
		this.searchDto=searchDto;
	}

	@Override
	public Predicate toPredicate(Root<Department> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate>predicate =new ArrayList<Predicate>();
		List<Predicate>orPredicates =new ArrayList<Predicate>();
		Predicate[] predicateArray=new Predicate[predicate.size()];
		Predicate and=criteriaBuilder.and(predicate.toArray(predicateArray));
		Predicate finalPredicate=null;
		if(searchDto.getSearchkey()!=null) {
			orPredicates.add(criteriaBuilder.like(root.get("depeartmentId"), "%"+searchDto.getSearchkey()+"%"));
			orPredicates.add(criteriaBuilder.like(root.get("departmentName"), "%"+searchDto.getSearchkey()+"%"));
			Predicate[] orPredicateArray=new Predicate[predicate.size()];
			Predicate or=criteriaBuilder.or(orPredicates.toArray(orPredicateArray));
			finalPredicate=criteriaBuilder.and(and,or);
		}else {
			finalPredicate=and;
		}
		return criteriaBuilder.and(finalPredicate);
	}

}
