package com.manoj.secification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.manoj.model.BenefitCode;
import com.manoj.search.dto.BenefitSearchDto;

public class SpecificationBenefitSeaarch implements Specification<BenefitCode> {
	
	private static final long serialVersionUID = 1L;
	private BenefitSearchDto benefitSearchDto;

	public SpecificationBenefitSeaarch(BenefitSearchDto benefitSearchDto) {
		this.benefitSearchDto = benefitSearchDto;
	}

	@Override
	public Predicate toPredicate(Root<BenefitCode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate>predicate =new ArrayList<Predicate>();
		List<Predicate>orPredicates =new ArrayList<Predicate>();
		Predicate[] predicateArray=new Predicate[predicate.size()];
		Predicate and=criteriaBuilder.and(predicate.toArray(predicateArray));
		Predicate finalPredicate=null;
		
		if(benefitSearchDto.getSearchKey()!=null) {
			orPredicates.add(criteriaBuilder.like(root.get("benefitId"), "%" + benefitSearchDto.getSearchKey() + "%"));
			orPredicates.add(criteriaBuilder.like(root.get("benefitCode"), "%" + benefitSearchDto.getSearchKey() + "%"));
			orPredicates.add(criteriaBuilder.like(root.get("benefitDesc"), "%" + benefitSearchDto.getSearchKey() + "%"));
			Predicate[] orPredicateArray=new Predicate[predicate.size()];
			Predicate or=criteriaBuilder.or(orPredicates.toArray(orPredicateArray));
			finalPredicate=criteriaBuilder.and(and,or);
		}else {
			finalPredicate=and;
		}
		return criteriaBuilder.and(finalPredicate);
	}

}
