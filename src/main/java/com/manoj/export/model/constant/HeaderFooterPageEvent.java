package com.manoj.export.model.constant;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class HeaderFooterPageEvent extends PdfPageEventHelper {

	private static final String DATE_FORMATE = "dd/MM/yyyy";

	private String empName;

	@Override
	public void onStartPage(PdfWriter writer, Document document) {
		ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(empName),
				EmployeeConsPDF.HUNDRED, EmployeeConsPDF.EIGHT_HUNDRED, EmployeeConsPDF.ZERO);
	}

	@Override
	public void onEndPage(PdfWriter writer, Document document) {
		ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
				new Phrase("page" + document.getPageNumber()), EmployeeConsPDF.FIVE_FIFTY, EmployeeConsPDF.THIRTY,
				EmployeeConsPDF.ZERO);

		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMATE);
		String date = format.format(new Date());

		ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT, new Phrase(date),
				EmployeeConsPDF.THIRTY, EmployeeConsPDF.THIRTY, EmployeeConsPDF.ZERO);

	}

}
