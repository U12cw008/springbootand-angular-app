package com.manoj.export.dto;

import lombok.Data;

@Data
public class ExportpdfDto {
	private Integer id;
	private String fileName;
	private String fileType;
	private byte[] content;

}
