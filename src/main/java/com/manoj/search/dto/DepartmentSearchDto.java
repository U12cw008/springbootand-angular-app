package com.manoj.search.dto;

import lombok.Data;

@Data
public class DepartmentSearchDto {

	private String depeartmentId;
	private String departmentName;
	private String searchkey;
}
