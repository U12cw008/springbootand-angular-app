package com.manoj.dto;

import java.util.List;

import lombok.Data;

@Data
public class EmployeeDto {

	private Integer id;
	private String empId;
	private String empFirstName;
	private String empMidleName;
	private String emplLastName;
	private String empGender;
	private String empType;
	protected Boolean isDeleted;
	private DtoDepartment department;
	private Integer ids;
	private byte[] employeeImage;
	private List<EmployeeDto> content;
	public void setContent(Object writeCities) {
		// TODO Auto-generated method stub
		
	}


}
