package com.manoj.dto;

import lombok.Data;

@Data
public class ApplicantDTO {
	private Integer id;
	private String firstName;
	private String lastName;
	private String address;
	private String city;
	private String country;
	private String phone;
	private Boolean applicantType;
}
