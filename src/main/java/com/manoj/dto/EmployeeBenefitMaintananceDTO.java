package com.manoj.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Data;

@Data
public class EmployeeBenefitMaintananceDTO {

	private Integer id;
	private String benefitName;
	private LocalDate startDate;
	private LocalDate endDate;
	private BigDecimal benefitAmountPerYear;
	private BigDecimal benefitAmountPerMonth;
	private BenefitCodeDto benefitCodeDto;

}
