package com.manoj.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DtoDepartment {
	private Integer id;
	private String depeartmentId;
	private String departmentName;
	protected Boolean isDeleted;
}
