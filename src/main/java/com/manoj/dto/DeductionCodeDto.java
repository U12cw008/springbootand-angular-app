package com.manoj.dto;

import lombok.Data;

@Data
public class DeductionCodeDto {

	private Integer id;

	private String deductionId;
	private String deductionName;
	private String deductionCode;
	private String deductionDesc;
	private boolean isDeleted;
	private int ids;
}
