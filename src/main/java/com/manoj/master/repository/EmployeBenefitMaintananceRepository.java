package com.manoj.master.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manoj.model.EmployeeBenefitMaintanance;

@Repository
public interface EmployeBenefitMaintananceRepository extends JpaRepository<EmployeeBenefitMaintanance, Integer> {

}
