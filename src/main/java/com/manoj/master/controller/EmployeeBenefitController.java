package com.manoj.master.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manoj.dto.EmployeeBenefitMaintananceDTO;
import com.manoj.master.service.EmployeeBenefitMaintananService;

@RestController
@RequestMapping(value = "/empyeeBenefitController")
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class EmployeeBenefitController {

	@Autowired
	private EmployeeBenefitMaintananService employeeBenefitMaintananService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public EmployeeBenefitMaintananceDTO create(@RequestBody EmployeeBenefitMaintananceDTO benefitMaintananceDTO) {
		return employeeBenefitMaintananService.saveOrUpdate(benefitMaintananceDTO);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public EmployeeBenefitMaintananceDTO update(@RequestBody EmployeeBenefitMaintananceDTO benefitMaintananceDTO) {
		return employeeBenefitMaintananService.saveOrUpdate(benefitMaintananceDTO);
	}

	@RequestMapping(value = "/getAllDropDown", method = RequestMethod.GET)
	public List<EmployeeBenefitMaintananceDTO> getAllDropDown() {
		return employeeBenefitMaintananService.getAllDropDown();
	}

}
