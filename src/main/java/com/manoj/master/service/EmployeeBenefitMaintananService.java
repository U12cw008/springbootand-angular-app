package com.manoj.master.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.manoj.dto.BenefitCodeDto;
import com.manoj.dto.EmployeeBenefitMaintananceDTO;
import com.manoj.master.repository.EmployeBenefitMaintananceRepository;
import com.manoj.model.BenefitCode;
import com.manoj.model.EmployeeBenefitMaintanance;

@Service
public class EmployeeBenefitMaintananService {

	@Autowired
	private EmployeBenefitMaintananceRepository employeBenefitMaintananceRepository;

	@Transactional
	public EmployeeBenefitMaintananceDTO saveOrUpdate(EmployeeBenefitMaintananceDTO benefitMaintananceDTO) {
		BenefitCode benefitCode = new BenefitCode();
		EmployeeBenefitMaintanance benefitMaintanance = new EmployeeBenefitMaintanance();
		if (benefitMaintananceDTO.getId() != null && benefitMaintananceDTO.getId() > 0) {
			if (benefitMaintananceDTO.getBenefitCodeDto() != null) {
				benefitCode.setId(benefitMaintananceDTO.getBenefitCodeDto().getId());
				benefitCode.setBenefitCode(benefitMaintananceDTO.getBenefitCodeDto().getBenefitCode());
				benefitCode.setBenefitId(benefitMaintananceDTO.getBenefitCodeDto().getBenefitId());
				benefitCode.setBenefitDesc(benefitMaintananceDTO.getBenefitCodeDto().getBenefitDesc());
			}
			benefitMaintanance.setBenefitCode(benefitCode);
			benefitMaintanance.setId(benefitMaintananceDTO.getId());
			benefitMaintanance.setBenefitName(benefitMaintananceDTO.getBenefitName());
			benefitMaintanance.setBenefitAmountPerMonth(benefitMaintananceDTO.getBenefitAmountPerMonth());
			benefitMaintanance.setBenefitAmountPerYear(benefitMaintananceDTO.getBenefitAmountPerYear());
			benefitMaintanance.setStartDate(benefitMaintananceDTO.getStartDate());
			benefitMaintanance.setEndDate(benefitMaintananceDTO.getEndDate());
			employeBenefitMaintananceRepository.saveAndFlush(benefitMaintanance);
		} else {
			if (benefitMaintananceDTO.getBenefitCodeDto() != null) {
				benefitCode.setId(benefitMaintananceDTO.getBenefitCodeDto().getId());
				benefitCode.setBenefitCode(benefitMaintananceDTO.getBenefitCodeDto().getBenefitCode());
				benefitCode.setBenefitId(benefitMaintananceDTO.getBenefitCodeDto().getBenefitId());
				benefitCode.setBenefitDesc(benefitMaintananceDTO.getBenefitCodeDto().getBenefitDesc());
			}
			benefitMaintanance.setBenefitCode(benefitCode);
			benefitMaintanance.setBenefitName(benefitMaintananceDTO.getBenefitName());
			benefitMaintanance.setBenefitAmountPerMonth(benefitMaintananceDTO.getBenefitAmountPerMonth());
			benefitMaintanance.setBenefitAmountPerYear(benefitMaintananceDTO.getBenefitAmountPerYear());
			benefitMaintanance.setStartDate(benefitMaintananceDTO.getStartDate());
			benefitMaintanance.setEndDate(benefitMaintananceDTO.getEndDate());
			employeBenefitMaintananceRepository.saveAndFlush(benefitMaintanance);
		}
		return benefitMaintananceDTO;
	}
	@Transactional
	public List<EmployeeBenefitMaintananceDTO> getAllDropDown() {
		List<EmployeeBenefitMaintananceDTO> emplBenefitMaintananceDTOList = new ArrayList<>();
		List<EmployeeBenefitMaintanance> benefitMaintananceList = employeBenefitMaintananceRepository.findAll();
		if (benefitMaintananceList != null && !benefitMaintananceList.isEmpty()) {
			for (EmployeeBenefitMaintanance employeeBenefitMaintanance : benefitMaintananceList) {
				EmployeeBenefitMaintananceDTO emplBenefitMaintananceDTO = new EmployeeBenefitMaintananceDTO();
				BenefitCodeDto benefitCodeDto = new BenefitCodeDto();
				if (employeeBenefitMaintanance.getBenefitCode() != null) {
					benefitCodeDto.setBenefitCode(employeeBenefitMaintanance.getBenefitCode().getBenefitCode());
					benefitCodeDto.setBenefitDesc(employeeBenefitMaintanance.getBenefitCode().getBenefitDesc());
					benefitCodeDto.setBenefitId(employeeBenefitMaintanance.getBenefitCode().getBenefitId());
					benefitCodeDto.setId(employeeBenefitMaintanance.getBenefitCode().getId());
				}
				emplBenefitMaintananceDTO.setBenefitCodeDto(benefitCodeDto);
				emplBenefitMaintananceDTO.setBenefitName(employeeBenefitMaintanance.getBenefitName());
				emplBenefitMaintananceDTO
						.setBenefitAmountPerMonth(employeeBenefitMaintanance.getBenefitAmountPerMonth());
				emplBenefitMaintananceDTO.setBenefitAmountPerYear(employeeBenefitMaintanance.getBenefitAmountPerYear());
				emplBenefitMaintananceDTO.setStartDate(employeeBenefitMaintanance.getStartDate());
				emplBenefitMaintananceDTO.setEndDate(employeeBenefitMaintanance.getEndDate());
				emplBenefitMaintananceDTO.setId(employeeBenefitMaintanance.getId());
				emplBenefitMaintananceDTOList.add(emplBenefitMaintananceDTO);
			}
		}
		return emplBenefitMaintananceDTOList;
	}

}
