package com.manoj.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.manoj.dto.AddressDto;
import com.manoj.model.Address;
import com.manoj.repository.AddressRepository;
import com.manoj.search.dto.AddressSearchDto;
import com.manoj.secification.SpecificationSearch;

@Service
public class AddressService {

	@Autowired
	private AddressRepository addressRepository;

	public AddressDto saveOrUpdate(AddressDto addressDto) {
		Address address = new Address();
		if (addressDto.getId() != null && address.getId() > 0) {
			address.setId(addressDto.getId());
			address.setCityName(addressDto.getCityName());
			address.setCountryId(addressDto.getCountryId());
			address.setCountryName(addressDto.getCountryName());
			address.setStateName(addressDto.getStateName());
			address.setZepCode(addressDto.getZepCode());
			address.setDeleted(Boolean.FALSE);
			addressRepository.saveAndFlush(address);
		} else {
			address.setCityName(addressDto.getCityName());
			address.setCountryId(addressDto.getCountryId());
			address.setCountryName(addressDto.getCountryName());
			address.setStateName(addressDto.getStateName());
			address.setZepCode(addressDto.getZepCode());
			address.setDeleted(Boolean.FALSE);
			addressRepository.saveAndFlush(address);
		}
		return addressDto;
	}

	public List<AddressDto> getAllDropDown() {
		List<AddressDto> addressDtoList = new ArrayList<AddressDto>();
		List<Address> addressList = addressRepository.findAll();
		if (addressList != null && !addressList.isEmpty()) {
			for (Address address : addressList) {
				AddressDto addressDto = new AddressDto();
				addressDto.setId(address.getId());
				addressDto.setCityName(address.getCityName());
				addressDto.setCountryId(address.getCountryId());
				addressDto.setCountryName(address.getCountryName());
				addressDto.setStateName(address.getStateName());
				addressDto.setZepCode(address.getZepCode());
				addressDto.setDeleted(address.isDeleted());
				addressDtoList.add(addressDto);
			}
		}
		return addressDtoList;
	}

	public AddressDto delete(AddressDto addressDto) {
		Address address = addressRepository.findOne(addressDto.getId());
		if (address != null) {
			addressDto.setId(address.getId());
			addressRepository.deleteById(true, address.getId());
		}
		return addressDto;
	}

	public Page<AddressDto> searchAllData(AddressSearchDto searchDto, Pageable pageable) {
		Page<Address> addressList = addressRepository.findAll(new SpecificationSearch(searchDto), pageable);
		List<AddressDto> content = addressList.getContent().stream().map(this::toDto).collect(Collectors.toList());
		return new PageImpl<>(content, pageable, addressList.getTotalElements());
	}

	public AddressDto toDto(Address address) {
		AddressDto addressDto = new AddressDto();
		addressDto.setId(address.getId());
		addressDto.setCityName(address.getCityName());
		addressDto.setCountryId(address.getCountryId());
		addressDto.setCountryName(address.getCountryName());
		addressDto.setStateName(address.getStateName());
		addressDto.setZepCode(address.getZepCode());
		addressDto.setDeleted(address.isDeleted());
		return addressDto;
	}

	public List<AddressDto> deleteList(List<Integer> ids) {
		List<AddressDto> addressDtosList = new ArrayList<>();
		for (Integer id : ids) {
			Address address = addressRepository.findOne(id);
			AddressDto addressDto = new AddressDto();
			addressDto.setCountryId(address.getCountryId());
			addressRepository.deleteSinglerecord(true, address.getId());
			addressDtosList.add(addressDto);
		}

		return addressDtosList;
	}

	public List<AddressDto> getAllDopDownFOrJava8() {
		List<AddressDto> addressDtosList = new ArrayList<>();
		List<Address> list = addressRepository.findAll();
		if (!list.isEmpty() && list != null) {
			List<Address> list2 =list.stream().sorted(Comparator.comparingInt(Address::getId).reversed()).collect(Collectors.toList());
			list2.forEach(obj -> {
				AddressDto addressDto = new AddressDto();
				getAddressDetails(addressDto, obj);
				addressDtosList.add(addressDto);

			});
		}
		return addressDtosList;
	}

	private void getAddressDetails(AddressDto addressDto, Address obj) {
		getData(addressDto, obj);
	}

	public AddressDto getById(Integer id) throws IOException {
		AddressDto addressDto = new AddressDto();
		Address address = addressRepository.findOne(id);
		if (address == null) {
			throw new IOException("record not found");
		} else {
			getData(addressDto, address);
		}
		return addressDto;
	}

	private void getData(AddressDto addressDto, Address address) {
		addressDto.setCityName(address.getCityName());
		addressDto.setId(address.getId());
		addressDto.setCityName(address.getCityName());
		addressDto.setCountryId(address.getCountryId());
		addressDto.setCountryName(address.getCountryName());
		addressDto.setStateName(address.getStateName());
		addressDto.setZepCode(address.getZepCode());
		addressDto.setDeleted(address.isDeleted());
	}

	public List<AddressDto> getAllDopDownFOrJava8ForEach() {
		List<AddressDto>addressDtosList =new ArrayList<>();
		List<Address> list = addressRepository.findAll();
		List<Address> list2=	list.stream().filter((obj)-> obj.getCityName().length() > 5).collect(Collectors.toList());
		list2.forEach(obj->{
			AddressDto addressDto = new AddressDto();
			getAddressDetails(addressDto, obj);
			addressDtosList.add(addressDto);
		});
		return addressDtosList;
	}

	//distinct method(remove duplicate object)
	public List<AddressDto> getAllDopDownFOrJava8ForDistinct() {
		List<AddressDto>addressDtosList =new ArrayList<>();
		List<Address> list = addressRepository.findAll();
		@SuppressWarnings({ "unchecked", "rawtypes" })
		List<Address> list2 =(List<Address>) list.stream().distinct().collect(Collectors.toCollection(() -> new TreeSet(Comparator.comparing(Address::getCityName)))).stream().collect(Collectors.toList());
		list2.forEach(obj ->{
			AddressDto addressDto = new AddressDto();
			getAddressDetails(addressDto, obj);
			addressDtosList.add(addressDto);
		});
		return addressDtosList;
	}

	// skip method (remove first 4 object)
	public List<AddressDto> getAllDopDownFOrJava8ForSkip() {
		List<AddressDto> addressDtosList = new ArrayList<>();
		List<Address> list = addressRepository.findAll();
		List<Address> list2 = list.stream().skip(4).collect(Collectors.toList());
		list2.forEach(obj -> {
			AddressDto addressDto = new AddressDto();
			getAddressDetails(addressDto, obj);
			addressDtosList.add(addressDto);
		});
		return addressDtosList;
	}
	
	// Limit method (remove first 4 object)
	public List<AddressDto> getAllDopDownFOrJava8ForLimit() {
		List<AddressDto> addressDtosList = new ArrayList<>();
		List<Address> list = addressRepository.findAll();
		List<Address> list2 = list.stream().limit(4).collect(Collectors.toList());
		list2.forEach(obj -> {
			AddressDto addressDto = new AddressDto();
			getAddressDetails(addressDto, obj);
			addressDtosList.add(addressDto);
		});
		return addressDtosList;
	}

	// count() method (count obj which length is greterthan 4)
	public List<AddressDto> getAllDopDownFOrJava8ForCountLength() {
		List<AddressDto> addressDtosList = new ArrayList<>();
		List<Address> list = addressRepository.findAll();
		long list2 = list.stream().filter(obj -> obj.getCityName().length() > 4).count();
		AddressDto addressDto = new AddressDto();
		addressDto.setCount((int) list2);
		addressDtosList.add(addressDto);
		return addressDtosList;
	}


}
