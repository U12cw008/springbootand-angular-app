package com.manoj.service.exception;

public class StorageException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3572514096978988635L;

	public StorageException(String message) {
        super(message);
    }

    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }

}
