package com.manoj.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.manoj.dto.ApplicantDTO;
import com.manoj.model.Applicant;
import com.manoj.repository.ApplicantRepository;

@Service
public class ApplicantService {

	@Autowired
	private ApplicantRepository applicantRepository;

	public ApplicantDTO saveOrUpdate(ApplicantDTO applicantDTO) {
		Applicant applicant = new Applicant();
		if (applicantDTO.getId() != null && applicantDTO.getId() > 0) {
			applicant.setId(applicantDTO.getId());
			applicant.setAddress(applicantDTO.getAddress());
			applicant.setApplicantType(applicantDTO.getApplicantType());
			applicant.setCity(applicantDTO.getCity());
			applicant.setCountry(applicantDTO.getCountry());
			applicant.setFirstName(applicantDTO.getFirstName());
			applicant.setLastName(applicantDTO.getLastName());
			applicant.setPhone(applicantDTO.getPhone());
			applicantRepository.saveAndFlush(applicant);
		} else {
			applicant.setAddress(applicantDTO.getAddress());
			applicant.setApplicantType(applicantDTO.getApplicantType());
			applicant.setCity(applicantDTO.getCity());
			applicant.setCountry(applicantDTO.getCountry());
			applicant.setFirstName(applicantDTO.getFirstName());
			applicant.setLastName(applicantDTO.getLastName());
			applicant.setPhone(applicantDTO.getPhone());
			applicantRepository.saveAndFlush(applicant);
		}
		return applicantDTO;
	}

	public List<ApplicantDTO> getAllDropDown() {
		ArrayList<ApplicantDTO> applicantDTOList = new ArrayList<>();
		List<Applicant> applicantList = applicantRepository.findAll();
		if (applicantList != null && !applicantList.isEmpty()) {
			for (Applicant applicant : applicantList) {
				ApplicantDTO applicantDTO = new ApplicantDTO();
				applicantDTO.setId(applicant.getId());
				applicantDTO.setAddress(applicant.getAddress());
				applicantDTO.setApplicantType(applicant.getApplicantType());
				applicantDTO.setCity(applicant.getCity());
				applicantDTO.setCountry(applicant.getCountry());
				applicantDTO.setFirstName(applicant.getFirstName());
				applicantDTO.setLastName(applicant.getLastName());
				applicantDTO.setPhone(applicant.getPhone());
				applicantDTOList.add(applicantDTO);
			}
		}
		return applicantDTOList;
	}

}
