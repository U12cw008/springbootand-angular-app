package com.manoj.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lowagie.text.DocumentException;
import com.manoj.dto.EmployeeDto;
import com.manoj.export.dto.ExportpdfDto;
import com.manoj.search.dto.EmployeeSearchDto;
import com.manoj.service.EmployeeService;

@RestController
@RequestMapping(value = "/employeeController")
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public EmployeeDto create(HttpServletRequest request, @RequestParam("EmployeeDto") String employeeDto,
			@RequestParam(name = "file", required = false) MultipartFile file) throws IOException
			 {
		ObjectMapper objectMapper = new ObjectMapper();
		EmployeeDto employeeDto2 = objectMapper.readValue(employeeDto, EmployeeDto.class);
		return employeeService.saveOrUpdate(employeeDto2,file);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public EmployeeDto update(HttpServletRequest request, @RequestParam("EmployeeDto") String employeeDto,
			@RequestParam(name = "file", required = false) MultipartFile file)
			throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		EmployeeDto employeeDto2 = objectMapper.readValue(employeeDto, EmployeeDto.class);
		return employeeService.saveOrUpdate(employeeDto2,file);
	}


	@RequestMapping(value = "/getAllDropDown", method = RequestMethod.GET)
	public List<EmployeeDto> getAllDropDown() {
		return employeeService.getAllDropDown();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public EmployeeDto delete(@RequestBody EmployeeDto employeeDto) {
		return employeeService.delete(employeeDto);
	}

	@RequestMapping(value = "/deleteList", method = RequestMethod.POST)
	public List<EmployeeDto> deleteList(@RequestBody List<Integer> ids) {
		return employeeService.deleList(ids);
	}

	@RequestMapping(value = "/getById", method = RequestMethod.POST)
	public EmployeeDto getById(@RequestBody EmployeeDto employeeDto) {
		return employeeService.getById(employeeDto);
	}

	@RequestMapping(value = "/searchEmployee", method = RequestMethod.POST)
	public Page<EmployeeDto> searchEmployee(@RequestBody EmployeeSearchDto searchDto, Pageable pageable) {
		return employeeService.searchEmployee(searchDto, pageable);
	}

	@RequestMapping(value = "/exportPdf", method = RequestMethod.POST)
	public ResponseEntity<Object> exportEmployee(EmployeeDto employeeDto) throws DocumentException {
		ExportpdfDto exportpdfDto = employeeService.exportPDF(employeeDto.getId());
		return new ResponseEntity<Object>(exportpdfDto.getContent(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/exportCSV/{id}", method = RequestMethod.GET)
	public void csvForId(@PathVariable Integer id,HttpServletResponse response) throws IOException {
		 employeeService.csvForId(response,id);
	}
	
	@RequestMapping(value = "/exportCSV", method = RequestMethod.GET)
	public void findAllCitiesCsv(HttpServletResponse response) throws IOException {
		 employeeService.findAllCitiesCsv(response);
	}
}
