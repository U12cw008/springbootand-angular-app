package com.manoj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manoj.dto.ApplicantDTO;
import com.manoj.service.ApplicantService;

@RestController
@RequestMapping(value = "/applicantController")
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class ApplicantController {

	@Autowired
	private ApplicantService applicantService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ApplicantDTO create(@RequestBody ApplicantDTO applicantDTO) {
		return applicantService.saveOrUpdate(applicantDTO);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ApplicantDTO update(@RequestBody ApplicantDTO applicantDTO) {
		return applicantService.saveOrUpdate(applicantDTO);
	}

	@RequestMapping(value = "/getAllDropDown", method = RequestMethod.GET)
	public List<ApplicantDTO> getAllDropDown() {
		return applicantService.getAllDropDown();
	}
}
