package com.manoj.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manoj.dto.AddressDto;
import com.manoj.search.dto.AddressSearchDto;
import com.manoj.service.AddressService;

@RestController
@RequestMapping(value = "/addressController")
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class AddressController {

	@Autowired
	private AddressService addressService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public AddressDto create(@RequestBody AddressDto addressDto) {
		return addressService.saveOrUpdate(addressDto);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public AddressDto update(@RequestBody AddressDto addressDto) {
		return addressService.saveOrUpdate(addressDto);
	}

	@RequestMapping(value = "/getAllDropDown", method = RequestMethod.GET)
	public List<AddressDto> getAllDopDown() {
		return addressService.getAllDropDown();
	}

	@RequestMapping(value = "/getAllDopDownFOrJava8", method = RequestMethod.GET)
	public List<AddressDto> getAllDopDownFOrJava8() {
		return addressService.getAllDopDownFOrJava8();
	}
	
	@RequestMapping(value = "/getAllDopDownFOrJava8ForEach", method = RequestMethod.GET)
	public List<AddressDto> getAllDopDownFOrJava8ForEach() {
		return addressService.getAllDopDownFOrJava8ForEach();
	}

	@RequestMapping(value = "/getAllDopDownFOrJava8ForDistinct", method = RequestMethod.GET)
	public List<AddressDto> getAllDopDownFOrJava8ForDistinct() {
		return addressService.getAllDopDownFOrJava8ForDistinct();
	}

	@RequestMapping(value = "/getAllDopDownFOrJava8ForSkip", method = RequestMethod.GET)
	public List<AddressDto> getAllDopDownFOrJava8ForSkip() {
		return addressService.getAllDopDownFOrJava8ForSkip();
	}

	@RequestMapping(value = "/getAllDopDownFOrJava8ForLimit", method = RequestMethod.GET)
	public List<AddressDto> getAllDopDownFOrJava8ForLimit() {
		return addressService.getAllDopDownFOrJava8ForLimit();
	}

	@RequestMapping(value = "/getAllDopDownFOrJava8ForCountLength", method = RequestMethod.GET)
	public List<AddressDto> getAllDopDownFOrJava8ForCountLength() {
		return addressService.getAllDopDownFOrJava8ForCountLength();
	}

	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public AddressDto delete(@RequestBody AddressDto addressDto) {
		return addressService.delete(addressDto);
	}

	@RequestMapping(value = "/deleteList", method = RequestMethod.POST)
	public List<AddressDto> deleteList(@RequestBody AddressDto addressDto) {
		return addressService.deleteList(addressDto.getIds());
	}

	@RequestMapping(value = "/searchData", method = RequestMethod.POST)
	public Page<AddressDto> searchAllData(@RequestBody AddressSearchDto searchDto, Pageable pageable) {
		return addressService.searchAllData(searchDto, pageable);
	}
	@RequestMapping(value = "/getByIdFOrJava8", method = RequestMethod.POST)
	public AddressDto getById(@RequestBody AddressDto addressDto) throws IOException {
		return addressService.getById(addressDto.getId());
	}
}
